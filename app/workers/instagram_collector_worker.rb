class InstagramCollectorWorker
  include Sidekiq::Worker

  def perform(*args)
    client = Instagram.client(:access_token => InstagramAccessToken.last.try(:access_token), client_id: ENV['instagram_client_id'])
    photos = client.user_recent_media
    if photos.present?
      InstagramPhoto.delete_all
      photos.reverse.each do |image|
        InstagramPhoto.create_with(image_url: image[:images][:standard_resolution][:url], link: image.link).find_or_create_by(post_id: image.id)
      end
    end
  end

end
