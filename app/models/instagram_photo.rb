class InstagramPhoto < ApplicationRecord

  def self.get_last_photos
    if self.count == 0
      InstagramCollectorWorker.perform_async
      self.last(10).reverse
    elsif self.last.present? && self.last.created_at < 1.hour.ago
      InstagramCollectorWorker.perform_async
      self.last(10).reverse
    else
      self.last(10).reverse
    end
  end

end
