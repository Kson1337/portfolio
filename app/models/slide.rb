class Slide < ApplicationRecord
  mount_uploader :image, SlideUploader

  enum position: Option.get_options('slide_positions')

  scope :homepage_slides, -> { where(position: 0) }

end
