class Page < ApplicationRecord
  mount_uploader :header_image, PagesUploader

  validates :name, uniqueness: true

  enum name: Option.get_options('pages')

  scope :newborns_page,  -> { where(name: 0).first }
  scope :families_page,  -> { where(name: 1).first }
  scope :kids_page,      -> { where(name: 2).first }
  scope :pregnancy_page, -> { where(name: 3).first }
  scope :home_page,      -> { where(name: 4).first }
  scope :services_page,  -> { where(name: 5).first }
  scope :contacts_page,  -> { where(name: 6).first }
  scope :feedbacks_page, -> { where(name: 7).first }
  scope :studio_page,    -> { where(name: 8).first }

end
