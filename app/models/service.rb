class Service < ApplicationRecord

  enum category: Option.get_options('service_categories')

  validates :name, presence: true
  validates :category, presence: true
  validates :description, presence: true

  scope :pregnancy_services, -> { where(category: 0) }
  scope :newborn_services, -> { where(category: 1) }
  scope :family_services, -> { where(category: 2) }

end
