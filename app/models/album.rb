class Album < ApplicationRecord
  has_many :photos, dependent: :destroy

  validates :display_as, presence: true
  validates :name, presence: true

  enum category: Option.get_options('categories')
  enum display_as: Option.get_options('display_as')

  accepts_nested_attributes_for :photos, allow_destroy: true

  scope :pregnancy_albums, -> { includes(:photos).where(category: 0, display: true ).where.not(photos: {id: nil}).order(id: :asc) }
  scope :newborn_albums, -> { includes(:photos).where(category: 1, display: true ).where.not(photos: {id: nil}).order(id: :asc) }
  scope :kid_albums, -> { includes(:photos).where(category: 2, display: true ).where.not(photos: {id: nil}).order(id: :asc) }
  scope :family_albums, -> { includes(:photos).where(category: 3, display: true).where.not(photos: {id: nil}).order(id: :asc) }

  scope :photo_book_albums, -> { includes(:photos).where(category: 4, display: true ).where.not(photos: {id: nil}).order(id: :asc) }
  scope :studio_albums, -> { includes(:photos).where(category: 5, display: true ).where.not(photos: {id: nil}).order(id: :asc) }

  scope :albums_as_album, -> { where(display_as: 0) }
  scope :albums_as_carousel, -> { where(display_as: 1) }

  def cover
    photos.where(album_cover: true).first
  end

end
