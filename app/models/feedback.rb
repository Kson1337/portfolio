class Feedback < ApplicationRecord
  has_many :photos, dependent: :destroy

  accepts_nested_attributes_for :photos, allow_destroy: true

  validates :customer_name, presence: true
  validates :body, presence: true

  scope :published, -> { where(published: true) }

  def publish!
    self.update_attributes(published: true) unless published
  end


end
