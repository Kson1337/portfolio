class Photo < ApplicationRecord
  mount_uploader :image, PhotoUploader

  belongs_to :album, optional: true
  belongs_to :feedback, optional: true

  scope :feedback_photos, -> { where.not(feedback_id: nil) }

  def make_as_album_cover
    unless album_cover
      Album.find(album_id).photos.update_all(album_cover: false)
      self.update_attributes(album_cover: true)
    end
  end

end
