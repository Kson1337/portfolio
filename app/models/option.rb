class Option < ApplicationRecord

  OPTIONS = {

    yes_or_no: {
      'Да' => true,
      'Нет' => false
    },

    categories: {
      'Беременность' => 0,
      'Новорожденные' => 1,
      'Дети до года' => 2,
      'Семьи' => 3,
      'Фотокниги' => 4,
      'Mimi studio' => 5
    },

    service_categories: {
      'Беременность' => 0,
      'Новорожденные' => 1,
      'Семьи' => 2
    },

    display_as: {
      'Альбом' => 0,
      'Карусель' => 1
    },

    slide_positions: {
      'Домашняя страница' => 0
    },

    pages: {
      'Новорожденные' => 0,
      'Семьи' => 1,
      'Дети до года' => 2,
      'Беременность' => 3,
      'Домашняя страница' => 4,
      'Услуги' => 5,
      'Контакты' => 6,
      'Отзывы' => 7,
      'Mimi studio' => 8
    }

  }

  def self.get_options(type)
    self::OPTIONS[type.to_sym]
  end

end
