//= require jquery
//= require rails-ujs
//= require bootstrap-sprockets
//= require slick
//= require lightbox

//= require front_partials/navigation_bar
//= require front_partials/album_carousel
//= require services
//= require contacts

$(window).load(function(){
  $('.loading-icon').fadeOut(200);
})

$(document).ready(function() {

  $('.carousel').carousel({
    interval: 5000
  })

  $(window).scroll(function(){
    if ($(this).scrollTop() > 400) {
      $(".scrollToTop").fadeIn(150)
    } else {
      $(".scrollToTop").fadeOut(150);
    }
  });

  $(".scrollToTop").click(function(){
    $('html, body').animate({scrollTop : 0},500);
    return false;
  });

});