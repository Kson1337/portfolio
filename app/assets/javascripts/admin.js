//= require jquery
//= require rails-ujs
//= require bootstrap-sprockets
//= require metis-menu
//= require sb-admin-2
//= require trix
//= require admin/photos
//= require admin/instagram
//= require admin/feedbacks
//= require jquery.dataTables.min
//= require dataTables.bootstrap
//= require dataTables.responsive

$(document).ready(function() {
  $('#dataTable').dataTable();
} );
