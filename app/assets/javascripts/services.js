$(document).ready(function(){

  $('.services-sub-menu li').click(function(){
    var target_id = $(this).data('target')
    $('.services-sub-menu').find('.active').removeClass('active');
    $(this).addClass('active');

    if ($(this).data('target') !== $('.services').find('.active').attr('id')) {
      $('.services').find('.active').removeClass('active')
                                    .fadeOut('fast')
                                    .promise()
                                    .done(function(){
                                      $('#' + target_id).addClass('active')
                                                        .fadeIn('fast');
                                    });
    };
  });

  $('.order-button-wrapper .button').click(function(){
    var service_category = $('.services-sub-menu li.active').text().trim()
    var service_name = $(this).closest('.service')
                              .prev('.service-header')
                              .find('.header-text')
                              .text().trim().toLowerCase();
    var chosen_service = service_category + ' - ' + 'пакет «' + service_name + '»'
    $(this).attr('href', '/contacts?chosen_service=' + chosen_service);
  })


  //TODO ajax:success fires on each responce. Need to rework this ajax in the future

  $(document).on("ajax:success", function(){
    if ($('.alert.alert-danger').length == 0) {
      $('#contact_form_name').val('');
      $('#contact_form_email').val('');
      $('#contact_form_service').val('');
      $('#contact_form_message').val('');
    }
  })

  function startMenuHighlight() {
    setInterval(function(){
      $('.services-sub-menu ul li').not('.active').css('transition', 'all 3s').toggleClass('wannabe-active')
    }, 3000);
  };

  $('.services-sub-menu ul li').on('click mouseenter mouseleave', function(){
    $('.services-sub-menu ul li').css('transition', 'all 0s');
    $('.services-sub-menu ul li').removeClass('wannabe-active');
    clearInterval(startMenuHighlight)
  });

  $('.services-sub-menu ul li').on({
    mouseenter: function(){
      $(this).addClass('active-hover')
    },
    mouseleave: function(){
      $(this).removeClass('active-hover')
    }
  })

  startMenuHighlight();

});