class FeedbacksController < ApplicationController
  include ActsAsTaggable

  before_action :set_page
  before_action -> { set_page_meta_tags(@page) }

  def index
    @feedback = Feedback.new
    @published_feedbacks = Feedback.published.order('created_at desc').paginate(page: params[:page], per_page: 10)
    @page = Page.feedbacks_page
  end

  def create
    @feedback = Feedback.new feedback_params
    respond_to do |format|
      if verify_recaptcha(model: @feedback) && @feedback.save
        format.html { render :index }
        format.js   { flash.now[:success] = @message = "Спасибо за Ваш отзыв! Мы опубликуем его уже очень скоро!" }
      elsif !verify_recaptcha(model: @feedback)
        format.html { render :index }
        format.js   { flash.now[:danger] = @message = "Пожалуйста пройдите капчу" }
      else
        format.html { render :index }
        format.js   { flash.now[:danger] = @message = "Пожалуйста заполните все поля" }
      end
    end
  end

  private

  def feedback_params
    params.require(:feedback).permit(:customer_name, :body, photos_attributes: [:image, :_destroy])
  end

  def set_page
    @page = Page.feedbacks_page
  end

end
