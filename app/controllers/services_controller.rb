class ServicesController < ApplicationController
  include ActsAsTaggable

  before_action :set_page
  before_action -> { set_page_meta_tags(@page) }


  def index
    @carousels = Album.albums_as_carousel.photo_book_albums

    newborn_services = Service.newborn_services.order(:position)
    family_services = Service.family_services.order(:position)
    pregnancy_services = Service.pregnancy_services.order(:position)

    @all_services = [
                      pregnancy_services,
                      newborn_services,
                      family_services
                    ]
  end

  private

  def set_page
    @page = Page.services_page
  end
end
