class StudioController < ApplicationController
  include ActsAsTaggable

  before_action :set_page
  before_action -> { set_page_meta_tags(@page) }

  def index
    @album = Album.albums_as_carousel.studio_albums
  end

  private

  def set_page
    @page = Page.studio_page
  end
end
