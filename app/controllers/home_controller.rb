class HomeController < ApplicationController
  include ActsAsTaggable

  before_action :set_page
  before_action -> { set_page_meta_tags(@page) }

  def index
    @slides = Slide.homepage_slides.order(created_at: :asc)
    @instagram_photos = InstagramPhoto.get_last_photos

    @newborns_gallery_photo = Album.newborn_albums.last.try(:cover)
    @pregnancy_gallery_photo = Album.pregnancy_albums.last.try(:cover)
    @kids_gallery_photo = Album.kid_albums.last.try(:cover)
    @families_gallery_photo = Album.family_albums.first.try(:cover)

    newborn_services = Service.newborn_services.order(:position)
    family_services = Service.family_services.order(:position)
    pregnancy_services = Service.pregnancy_services.order(:position)

    @all_services = [
                      pregnancy_services,
                      newborn_services,
                      family_services
                    ]
  end

  private

  def set_page
    @page = Page.home_page
  end

end
