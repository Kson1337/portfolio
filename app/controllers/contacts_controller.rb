class ContactsController < ApplicationController
  include ActsAsTaggable

  before_action :set_page
  before_action -> { set_page_meta_tags(@page) }

  def index
    @chosen_service = params[:chosen_service]
  end

  def create
    @contact_form = ContactForm.new contact_form_params
    @contact_form.request = request
    respond_to do |format|
      if @contact_form.deliver
        format.html { render :index }
        format.js   { flash.now[:success] = @message = "Спасибо за заявку. Я свяжусь с Вами в ближайшее время!" }
      else
        format.html { render :index }
        format.js   { flash.now[:danger] = @message = "Пожалуйста заполните все поля" }
      end
    end
  end

  private

  def contact_form_params
    params.require(:contact_form).permit(:name, :email, :service, :message)
  end

  def set_page
    @page = Page.contacts_page
  end
end
