module ActsAsTaggable
  extend ActiveSupport::Concern

  def set_page_meta_tags(page)
    set_meta_tags title: page.title,
                  description: page.description,
                  keywords: page.keywords
  end


end