class Portfolio::PregnancyController < ApplicationController
  include ActsAsTaggable

  before_action :set_page
  before_action -> { set_page_meta_tags(@page) }

  def index
    @albums = Album.albums_as_album.pregnancy_albums
    @carousels = Album.albums_as_carousel.pregnancy_albums
  end

  def show
    @album = Album.find params[:id]
  end

  private

  def set_page
    @page = Page.pregnancy_page
  end

end
