class Portfolio::FamiliesController < ApplicationController
  include ActsAsTaggable

  before_action :set_page
  before_action -> { set_page_meta_tags(@page) }

  def index
    @albums = Album.albums_as_album.family_albums
    @carousels = Album.albums_as_carousel.family_albums
  end

  def show
    @album = Album.find params[:id]
  end

  private

  def set_page
    @page = Page.families_page
  end

end
