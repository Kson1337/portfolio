class Portfolio::NewbornsController < ApplicationController
  include ActsAsTaggable

  before_action :set_page
  before_action -> { set_page_meta_tags(@page) }

  def index
    @albums = Album.albums_as_album.newborn_albums
    @carousels = Album.albums_as_carousel.newborn_albums
  end

  def show
    @album = Album.find params[:id]
  end

  private

  def set_page
    @page = Page.newborns_page
  end

end
