class Admin::PhotosController < Admin::BaseController

  before_action :find_photo, only: [:destroy, :show, :edit, :update]

  def index
    @album = Album.find(params[:album_id])
    @photos = @album.photos
  end

  def show
  end

  def edit
  end

  def update
    if @photo.update_attributes photo_params
      redirect_to admin_album_photos_path(@photo.album)
    else
      render :edit
    end
  end

  def destroy
    if @photo.destroy
      redirect_to admin_album_photos_path(@photo.album)
    end
  end

  def make_as_album_cover
    photo = Photo.find(params[:photo_id])
    photo.make_as_album_cover
    respond_to do |format|
      format.js { render js: "window.location.pathname='#{admin_album_photos_path(photo.album)}'" }
    end
  end

  private

  def photo_params

    params.require(:photo).permit(:name, :alt, :title, :image)

  end

  def find_photo
    @photo = Photo.find params[:id]
  end

end
