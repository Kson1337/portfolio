class Admin::ServicesController < Admin::BaseController

  before_action :find_service, only: [:show, :edit, :update, :edit, :destroy]

  def index
    @services = Service.all.order(:category)
  end

  def show
  end

  def new
    @service = Service.new
  end

  def create
    @service = Service.new service_params
    if @service.save
      redirect_to admin_services_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @service.update_attributes service_params
      redirect_to admin_services_path
    else
      render :edit
    end
  end

  def destroy
    if @service.destroy
      redirect_to admin_services_path
    end
  end

  private

  def find_service
    @service = Service.find(params[:id])
  end

  def service_params
    params.require(:service).permit(:name, :description, :position, :category, :price)
  end
end
