class Admin::SlidesController < Admin::BaseController

  before_action :find_slide, only: [:edit, :update, :destroy, :show]

  def index
    @homepage_slides = Slide.homepage_slides
  end

  def new
    @slide = Slide.new
  end

  def create
    @slide = Slide.new slide_params
    if @slide.save
      redirect_to admin_slides_path
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @slide.update_attributes(slide_params)
      redirect_to admin_slides_path
    else
      render :edit
    end
  end

  def destroy
    if @slide.destroy
      redirect_to admin_slides_path
    end
  end

  private

  def slide_params
    params.require(:slide).permit(:name, :caption_h1, :caption_body, :image, :position)
  end

  def find_slide
    @slide = Slide.find params[:id]
  end

end
