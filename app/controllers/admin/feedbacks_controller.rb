class Admin::FeedbacksController < Admin::BaseController

  before_action :find_feedback, only: [:edit, :update, :destroy, :show]

  def index
    @not_published_feedbacks = Feedback.where(published: false).paginate(page: params[:page], per_page: 10)
  end

  def new
    @feedback = Feedback.new
  end

  def create
    @feedback = Feedback.new feedback_params
    if @feedback.save
      redirect_to admin_feedbacks_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @feedback.update_attributes(feedback_params)
      redirect_to admin_feedbacks_path
    else
      render :edit
    end
  end

  def destroy
    if @feedback.destroy
      redirect_to admin_feedbacks_path
    end
  end

  def publish
    feedback = Feedback.find(params[:feedback_id])
    feedback.publish!
    respond_to do |format|
      format.js { render js: "window.location.pathname='#{admin_feedbacks_path}'" }
    end
  end

  def published_feedbacks
    @published_feedbacks = Feedback.published.paginate(page: params[:page], per_page: 10)
  end

  private

  def find_feedback
    @feedback = Feedback.find params[:id]
  end

  def feedback_params
    params.require(:feedback).permit(:customer_name, :body, photos_attributes: [:image, :_destroy])
  end

end
