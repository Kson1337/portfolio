class Admin::AlbumsController < Admin::BaseController

  before_action :find_album, only: [:edit, :update, :destroy, :show]

  def index
    @albums = Album.all
  end

  def new
    @album = Album.new
  end

  def create
    @album = Album.new album_params
    if @album.save
      redirect_to admin_album_photos_path(@album)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @album.update_attributes(album_params)
      redirect_to admin_albums_path
    else
      render :edit
    end
  end

  def destroy
    if @album.destroy
      redirect_to admin_albums_path
    end
  end

  private

  def album_params
    params.require(:album).permit(:name, :category, :display, :display_as, :video_url, photos_attributes: [:image, :_destroy])
  end

  def find_album
    @album = Album.find params[:id]
  end

end
