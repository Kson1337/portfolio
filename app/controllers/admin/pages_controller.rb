class Admin::PagesController < Admin::BaseController

  before_action :find_page, only: [:edit, :update]

  def index
    @pages = Page.all
  end

  def edit
  end

  def update
    if @page.update_attributes page_params
      redirect_to admin_pages_path
    else
      render :new
    end
  end

  private

  def page_params
    params.require(:page).permit :name,
                                 :header_image,
                                 :header_text,
                                 :page_content_heading_1,
                                 :page_content_body_1,
                                 :page_content_heading_2,
                                 :page_content_body_2,
                                 :page_content_heading_3,
                                 :page_content_body_3,
                                 :phone_number,
                                 :email,
                                 :facebook_url,
                                 :instagram_url,
                                 :vk_url,
                                 :address,
                                 :title,
                                 :description,
                                 :keywords
  end

  def find_page
    @page = Page.find params[:id]
  end

end
