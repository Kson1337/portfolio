class Admin::InstagramController < Admin::BaseController
  def index
  end

  def callback
    response = Instagram.get_access_token(params[:code], client_id: ENV['instagram_client_id'], client_secret: ENV['instagram_client_secret'], redirect_uri: ENV['instagram_redirect_uri'])
    if response
      InstagramAccessToken.create(access_token: response.access_token)
      redirect_to admin_instagram_index_path
    else
      render :index
    end
  end

  def update_photos_manually
    InstagramCollectorWorker.perform_async
    flash[:success] = 'Фотографии успешно обновлены'
    redirect_to admin_instagram_index_path
  end
end
