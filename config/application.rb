require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module Portfolio
  class Application < Rails::Application
    config.load_defaults 5.1

    config.autoload_paths += %W(#{config.root}/app/workers)
    config.active_job.queue_adapter = :sidekiq

  end
end
