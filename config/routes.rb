require 'sidekiq/web'

Rails.application.routes.draw do

  root 'home#index'

  devise_for :admins, :controllers => { :sessions => "admin/sessions" }

  resources :home, only: [:index]
  resources :services, only: [:index]
  resources :contacts, only: [:index, :create]
  resources :feedbacks, only: [:index, :create]
  resources :studio, only: [:index]

  namespace :portfolio do
    resources :newborns, only: [:index, :show]
    resources :kids, only: [:index, :show]
    resources :pregnancy, only: [:index, :show]
    resources :families, only: [:index, :show]
  end

  namespace :admin do
    authenticate :admin do
      mount Sidekiq::Web => '/sidekiq'
    end

    resources :home, only: [:index]
    resources :slides
    resources :services
    resources :albums do
      resources :photos do
        get 'make_as_album_cover', to: 'photos#make_as_album_cover'
      end
    end
    resources :pages
    resources :instagram, only: [:index]
    resources :feedbacks do
      get 'publish', to: 'feedbacks#publish'
    end

    get 'published_feedbacks', to: 'feedbacks#published_feedbacks'

    get 'instagram/callback' => 'instagram#callback'
    get 'instagram/update_photos_manually' => 'instagram#update_photos_manually'

  end

  get :admin, to: 'admin/home#index'

  # redirect to root on 404s
  get "*path" => redirect("/")

end
