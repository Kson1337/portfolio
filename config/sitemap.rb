# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://katasonova.com.ua"

SitemapGenerator::Sitemap.create do

  add contacts_path, :priority => 0.3, :changefreq => 'yearly'

  add services_path, :priority => 0.3, :changefreq => 'monthly'

  add '/portfolio/pregnancy', :priority => 0.5, :changefreq => 'monthly'

  Album.pregnancy_albums.find_each do |album|
    add portfolio_pregnancy_path(album), :priority => 0.3, :changefreq => 'weekly'
  end

  add '/porfolio/newborns', :priority => 0.5, :changefreq => 'monthly'

  Album.newborn_albums.find_each do |album|
    add portfolio_newborn_path(album), :priority => 0.3, :changefreq => 'weekly'
  end

  add '/porfolio/families', :priority => 0.5, :changefreq => 'monthly'

  Album.family_albums.find_each do |album|
    add portfolio_family_path(album), :priority => 0.3, :changefreq => 'weekly'
  end

  add '/porfolio/kids', :priority => 0.5, :changefreq => 'monthly'

  add '/studio', :priority => 0.5, :changefreq => 'monthly'

  Album.kid_albums.find_each do |album|
    add portfolio_kid_path(album), :priority => 0.3, :changefreq => 'weekly'
  end

  add '/feedbacks', :priority => 0.7, :changefreq => 'weekly'

end
