namespace :photos do

  task generate_alts_and_titles: :environment do |t, args|

    Album.newborn_albums.each do |album|
      if album.photos.any?
        album.photos.update_all(alt: 'фотосессии новорожденных Харьков, фотограф новорожденных в Харькове Даша Катасонова', title: 'фотосессии новорожденных Харьков, фотограф новорожденных в Харькове Даша Катасонова')
        puts 'newborn alts generated!'
      else
        puts 'album ' + a.name + ' has no photos ¯\_(ツ)_/¯'
      end
    end

    Album.family_albums.each do |album|
      if album.photos.any?
        album.photos.update_all(alt: 'семейные фотосессии Харьков, семейный фотограф в Харькове Даша Катасонова', title: 'семейные фотосессии Харьков, семейный фотограф в Харькове Даша Катасонова')
        puts 'family alts generated!'
      else
        puts 'album ' + a.name + ' has no photos ¯\_(ツ)_/¯'
      end
    end

    Album.kid_albums.each do |album|
      if album.photos.any?
        album.photos.update_all(alt: 'детские фотосессии Харьков, детский фотограф в Харькове Даша Катасонова', title: 'детские фотосессии Харьков, детский фотограф в Харькове Даша Катасонова')
        puts 'kid alts generated!'
      else
        puts 'album ' + a.name + ' has no photos ¯\_(ツ)_/¯'
      end
    end

    Album.pregnancy_albums.each do |album|
      if album.photos.any?
        album.photos.update_all(alt: 'фотосессии беременности Харьков, семейный фотограф в Харькове Даша Катасонова', title: 'фотосессии беременности Харьков, семейный фотограф в Харькове Даша Катасонова')
        puts 'pregnancy alts generated!'
      else
        puts 'album ' + a.name + ' has no photos ¯\_(ツ)_/¯'
      end
    end

    Album.photo_book_albums.each do |album|
      if album.photos.any?
        album.photos.update_all(alt: 'фотокниги, фото альбом, печать фотографий. Харьков', title: 'фотокниги, фото альбом, печать фотографий. Харьков')
        puts 'photobook alts generated!'
      else
        puts 'album ' + a.name + ' has no photos ¯\_(ツ)_/¯'
      end
    end

    Photo.feedback_photos.update_all(alt: 'отзывы Даша Катасонова, семейные фотосессии, фотосессия новорожденных, семейный фотограф, фотограф новорожденных харьков', title: 'отзывы Даша Катасонова, семейные фотосессии, фотосессия новорожденных, семейный фотограф, фотограф новорожденных харьков')

  end

end
