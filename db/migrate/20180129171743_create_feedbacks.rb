class CreateFeedbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :feedbacks do |t|
      t.string :customer_name
      t.string :body
      t.boolean :published, default: false

      t.timestamps
    end
  end
end
