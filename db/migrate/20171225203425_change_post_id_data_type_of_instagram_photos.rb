class ChangePostIdDataTypeOfInstagramPhotos < ActiveRecord::Migration[5.1]
  def change
    change_column :instagram_photos, :post_id, :string
  end
end
