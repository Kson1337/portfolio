class CreateSlides < ActiveRecord::Migration[5.1]
  def change
    create_table :slides do |t|
      t.string     :name
      t.string     :caption_h1
      t.string     :caption_body
      t.string     :image
      t.integer    :position
      t.timestamps
    end
  end
end
