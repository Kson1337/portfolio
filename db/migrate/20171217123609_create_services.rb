class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
      t.integer :category
      t.string  :name
      t.string  :description
      t.integer :position
      t.timestamps
    end
  end
end
