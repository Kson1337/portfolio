class CreatePhotos < ActiveRecord::Migration[5.1]
  def change
    create_table :photos do |t|
      t.belongs_to :album
      t.string     :name
      t.string     :alt
      t.string     :title
      t.string     :image
      t.boolean    :album_cover, default: false
      t.timestamps
    end
  end
end
