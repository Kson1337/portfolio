class CreateInstagramPhotos < ActiveRecord::Migration[5.1]
  def change
    create_table :instagram_photos do |t|
      t.string :image_url
      t.string :link

      t.timestamps
    end
  end
end
