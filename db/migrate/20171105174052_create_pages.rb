class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.integer :name
      t.string  :header_image
      t.string  :header_text
      t.string  :page_content_heading_1
      t.string  :page_content_body_1
      t.string  :page_content_heading_2
      t.string  :page_content_body_2
      t.string  :page_content_heading_3
      t.string  :page_content_body_3
      t.string  :phone_number
      t.string  :email
      t.string  :facebook_url
      t.string  :instagram_url
      t.string  :vk_url
      t.string  :address
      t.timestamps
    end
  end
end
