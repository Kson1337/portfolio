class AddPostIdToInstagramPhotos < ActiveRecord::Migration[5.1]
  def change
    add_column :instagram_photos, :post_id, :integer
  end
end
