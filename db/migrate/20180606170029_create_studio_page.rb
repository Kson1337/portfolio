class CreateStudioPage < ActiveRecord::Migration[5.1]
  def change
    Option.get_options('pages').each do |key, value|
      Page.create(name: value)
      puts "Page #{key} successfully created!"
    end
  end
end
