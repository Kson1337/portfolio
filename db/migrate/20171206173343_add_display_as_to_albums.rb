class AddDisplayAsToAlbums < ActiveRecord::Migration[5.1]
  def change
    add_column :albums, :display_as, :integer
  end
end
