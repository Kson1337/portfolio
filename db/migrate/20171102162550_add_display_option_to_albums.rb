class AddDisplayOptionToAlbums < ActiveRecord::Migration[5.1]
  def change
    add_column :albums, :display, :boolean, default: false
  end
end
