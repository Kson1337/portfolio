class AddPriceToService < ActiveRecord::Migration[5.1]
  def change
    add_column :services, :price, :integer, default: 0
  end
end
