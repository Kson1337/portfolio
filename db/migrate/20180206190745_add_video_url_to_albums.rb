class AddVideoUrlToAlbums < ActiveRecord::Migration[5.1]
  def change
    add_column :albums, :video_url, :string
  end
end
