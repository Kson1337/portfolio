class AddDescAndKeywordsToPage < ActiveRecord::Migration[5.1]
  def change
    add_column :pages, :keywords, :string
    add_column :pages, :description, :string
  end
end
